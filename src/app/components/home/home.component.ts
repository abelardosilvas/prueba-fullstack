import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LocalStorageService} from '../../core/local-storage/local-storage.service';
import {ADD_KEY} from '../../utils/common.protocols';
import {UserService} from '../../core/services/user.service';
import {Person} from '../../models/person.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  personList: Array<Person>;
  userForm: FormGroup;
  constructor(private fb: FormBuilder, private localStorage: LocalStorageService, private userService: UserService) {
    this.userForm = this.fb.group({name: ['', Validators.required]});
  }

  ngOnInit() {
    this.localStorage.setItem(ADD_KEY, []);
    this.userService.getPersonList().subscribe(response => {
      this.personList = response[`results`][0].result;
    }, error =>  {
    });
  }

  addUserHandler({ value, valid}: {value: string, valid: boolean}) {
    if (!valid) {
      return;
    }
    this.localStorage.updateItem(ADD_KEY, value);
  }

  get name() {
    return this.userForm.get('name');
  }

}
