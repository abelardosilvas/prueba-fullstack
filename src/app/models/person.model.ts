export interface Person {
  name: string;
  age: string;
  gender: string;
  documents: string;
}
