import {Injectable} from '@angular/core';
import _ from 'lodash';
import {ADD_KEY, APP_PREFIX} from '../../utils/common.protocols';

/*export interface Person {
  data: Array<{name: string}>;
}*/
@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor() {}

  setItem(key: string, value: any): void {
    const packedData: any = this.getItem(ADD_KEY);
    if (packedData && packedData.length > 0) {
      return;
    }
    localStorage.setItem(`${APP_PREFIX}${key}`, JSON.stringify(value));
  }

  getItem(key: string): void {
    return JSON.parse(localStorage.getItem(`${APP_PREFIX}${key}`));
  }

  updateItem(key: string, value: any): void {
    let packedData: any = this.getItem(key);
    if (packedData) {
      packedData = [...packedData, value];
      /*_.update(packedData, path, () => {
        return value;
      });*/
      localStorage.setItem(`${APP_PREFIX}${key}`, JSON.stringify(packedData));
    }
  }

  removeItem(key: string): void {
    localStorage.removeItem(`${APP_PREFIX}${key}`);
  }
}
