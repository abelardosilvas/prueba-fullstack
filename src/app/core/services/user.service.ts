import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  urlBase = 'https://randomapi.com/api/5cdqdh4z?key=G6YF-1SE2-1BDV-EQAU';
  constructor(private http: HttpClient) { }

  getPersonList() {
    return this.http.get(this.urlBase);
  }
}
